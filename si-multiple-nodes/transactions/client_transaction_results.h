#pragma once

#include <vector>

#include "types.h"

struct ClientTransactionResults {
  struct KeyValue {
    Key key;
    Value value;
  };

  TransactionId txid;

  Timestamp read_timestamp;
  Timestamp commit_timestamp;

  std::vector<KeyValue> gets;
  std::vector<KeyValue> puts;
};
