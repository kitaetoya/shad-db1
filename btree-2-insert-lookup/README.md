# B-Tree insert lookup

В этой задаче необходимо реализовать методы `insert` и `lookup` для BTree в файле `btree.h` и `btree.cpp`.

В результате должен работать тест `btree_2_insert_lookup.cpp`.
